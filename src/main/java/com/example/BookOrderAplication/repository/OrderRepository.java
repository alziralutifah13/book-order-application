package com.example.BookOrderAplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.BookOrderAplication.model.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

}
