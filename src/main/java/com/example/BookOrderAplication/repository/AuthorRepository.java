package com.example.BookOrderAplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.BookOrderAplication.model.Author;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long>{

}
