package com.example.BookOrderAplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.BookOrderAplication.model.Reviewer;

@Repository
public interface ReviewerRepository extends JpaRepository<Reviewer, Long>{

}
