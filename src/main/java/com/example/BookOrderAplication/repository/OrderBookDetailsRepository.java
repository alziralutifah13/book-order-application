package com.example.BookOrderAplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.BookOrderAplication.model.OrderBookDetails;
import com.example.BookOrderAplication.model.OrderDetailsKey;

@Repository
public interface OrderBookDetailsRepository extends JpaRepository<OrderBookDetails, OrderDetailsKey> {

}
