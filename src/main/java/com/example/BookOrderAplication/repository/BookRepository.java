package com.example.BookOrderAplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.BookOrderAplication.model.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

}
