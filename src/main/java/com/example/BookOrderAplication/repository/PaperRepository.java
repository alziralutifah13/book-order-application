package com.example.BookOrderAplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.BookOrderAplication.model.Paper;

@Repository
public interface PaperRepository extends JpaRepository<Paper, Long> {

}
