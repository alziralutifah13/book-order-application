package com.example.BookOrderAplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.BookOrderAplication.model.BookRatings;

@Repository
public interface BookRatingsRepository extends JpaRepository<BookRatings, Long>{

}
