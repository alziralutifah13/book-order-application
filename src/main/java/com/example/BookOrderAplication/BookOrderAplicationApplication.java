package com.example.BookOrderAplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
public class BookOrderAplicationApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookOrderAplicationApplication.class, args);
	}

}
