package com.example.BookOrderAplication.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderAplication.DTO.BookRatingsDTO;
import com.example.BookOrderAplication.DTO.OrderBookDetailsDTO;
import com.example.BookOrderAplication.model.Book;
import com.example.BookOrderAplication.model.BookRatings;
import com.example.BookOrderAplication.model.Order;
import com.example.BookOrderAplication.model.OrderBookDetails;
import com.example.BookOrderAplication.model.OrderDetailsKey;
import com.example.BookOrderAplication.model.Reviewer;
import com.example.BookOrderAplication.repository.BookRatingsRepository;
import com.example.BookOrderAplication.repository.BookRepository;
import com.example.BookOrderAplication.repository.ReviewerRepository;

@RestController
@RequestMapping("/api/book-ratings")
public class BookRatingsController {
	@Autowired
	BookRatingsRepository bookRatingsRepo;
	@Autowired
	BookRepository bookRepo;
	@Autowired
	ReviewerRepository reviewerRepo;
	ModelMapper mapper = new ModelMapper();
	
	@PostMapping("/create")
	public ResponseEntity<Object> createBookRatings(@Valid @RequestBody BookRatings body) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Create BookRatings Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Book entityBook = bookRepo.findById(body.getBook().getBookID()).orElse(null);
			Reviewer entityReviewer = reviewerRepo.findById(body.getReviewer().getReviewID()).orElse(null);
			
			if (entityBook == null) {
				message = "ERROR, Book ID tidak Ditemukan!!!";
				status = HttpStatus.NOT_FOUND;
				
				result.put("status", status);
				result.put("message", message);
				
				return ResponseEntity.status(status).body(result);
			}
			
			if (entityReviewer == null) {
				message = "ERROR, Reviewer ID tidak Ditemukan!!!";
				status = HttpStatus.NOT_FOUND;
				
				result.put("status", status);
				result.put("message", message);
				
				return ResponseEntity.status(status).body(result);
			}
	
			BookRatings entity = bookRatingsRepo.save(body);
			BookRatingsDTO dto = mapper.map(entity, BookRatingsDTO.class);
			
		
			result.put("message", message);
			result.put("status", status.value());
			result.put("data", dto);
			
			return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to create BookRatings");
			result.put("error", e);
			
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}
	}
	
	@GetMapping("/getall")
	public ResponseEntity<Object> getAllBookRatings() {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find All BookRatings Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			List<BookRatings> listAll = bookRatingsRepo.findAll();
			List<BookRatingsDTO> listDTO = new ArrayList<BookRatingsDTO>();
			
			//mapping data dari Entity ke DTO
			for (BookRatings entity : listAll) {
				BookRatingsDTO dto = mapper.map(entity, BookRatingsDTO.class);
		
				listDTO.add(dto);
			}
			
			
			if (listDTO.isEmpty()) {
				status = HttpStatus.NOT_FOUND;
				message = "OrderBookDetails is Empty";
				
				result.put("status", status);
				result.put("message", message);
				result.put("data", listDTO);
				
				return ResponseEntity.status(status).body(result);
			}
			
			result.put("status", status.value());
			result.put("message", message);
			result.put("data", listDTO);
			
			return ResponseEntity.status(status).body(result);
			
		} catch (Exception e) {
			result.put("message", "Failed to get BookRatings");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}		
	}
	
	@GetMapping("/get")
	public ResponseEntity<Object> getById(@Valid @RequestBody Long bookRatingId) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find BookRatings by Id Succes!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			BookRatings entity = bookRatingsRepo.findById(bookRatingId).orElse(null);
			
			 if (entity == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "BookRatings id : "+ bookRatingId + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
			 	BookRatingsDTO dto = mapper.map(entity, BookRatingsDTO.class);
				 
				result.put("status",  status.value());
	            result.put("message", message);
	            result.put("data", dto);
	            
				return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to get BookRatings");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateBookRatings(@Valid @PathVariable(value = "id") Long bookRatingId, @Valid @RequestBody BookRatings update) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Update BookRatings Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			BookRatings entity = bookRatingsRepo.findById(bookRatingId).orElse(null);
			
			 if (entity == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "BookRatings id : "+ bookRatingId + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
			 entity.setBookRatingId(update.getBookRatingId());
			 entity.setBook(update.getBook());
			 entity.setReviewer(update.getReviewer());
			 entity.setRatingScore(update.getRatingScore());
			 
			 bookRatingsRepo.save(entity);
			 BookRatingsDTO dto = mapper.map(entity, BookRatingsDTO.class);
			 
            result.put("status",  status.value());
            result.put("message", message);
            result.put("data", dto);
            return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to update BookRatings");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
		
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteBookRatings(@Valid @PathVariable(value = "id") Long bookRatingId) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Delete BookRatings Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			BookRatings entity = bookRatingsRepo.findById(bookRatingId).orElse(null);
			
			 if (entity == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "BookRatings id : "+ bookRatingId + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
	            
			bookRatingsRepo.delete(entity);
            result.put("status",  status.value());
            result.put("message", message);

            return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to delete BookRatings");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
	}
	

}
