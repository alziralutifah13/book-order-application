package com.example.BookOrderAplication.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderAplication.DTO.PaperDTO;
import com.example.BookOrderAplication.DTO.PublisherDTO;
import com.example.BookOrderAplication.model.Paper;
import com.example.BookOrderAplication.model.Publisher;
import com.example.BookOrderAplication.repository.PaperRepository;

@RestController
@RequestMapping("/api/paper")
public class PaperController {
	
	@Autowired
	PaperRepository paperRepo;
	
	ModelMapper mapper = new ModelMapper();
	
	@PostMapping("/create")
	public ResponseEntity<Object> createPaper(@Valid @RequestBody PaperDTO body) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Create Paper Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Paper paper = mapper.map(body, Paper.class);
			Paper save = paperRepo.save(paper);
			
			result.put("message", message);
			result.put("status", status.value());
			result.put("data", save);
			
			return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to create Paper");
			result.put("error", e);
			
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}
	}
	
	@GetMapping("/getall")
	public ResponseEntity<Object> getAllPaper() {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find All Paper Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			List<Paper> listAll = paperRepo.findAll();
			List<PaperDTO> listDTO = new ArrayList<PaperDTO>();
			
			for (Paper entity : listAll) {
				PaperDTO paperDto = mapper.map(entity, PaperDTO.class);
				
				listDTO.add(paperDto);
			}
			
			
			if (listDTO.isEmpty()) {
				status = HttpStatus.NOT_FOUND;
				message = "Paper is Empty";
				
				result.put("status", status);
				result.put("message", message);
				result.put("data", listDTO);
				
				return ResponseEntity.status(status).body(result);
			}
			
			result.put("status", status.value());
			result.put("message", message);
			result.put("data", listDTO);
			
			return ResponseEntity.status(status).body(result);
			
		} catch (Exception e) {
			result.put("message", "Failed to get Paper");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}		
	}
	
	@GetMapping("/get")
	public ResponseEntity<Object> getPaperById(@Valid @RequestParam(name = "id") Long paperID) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find Paper by Id Succes!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Paper entity = paperRepo.findById(paperID).orElse(null);
			
			 if (entity == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Paper id : "+ paperID + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
	
			 PaperDTO paperDto = mapper.map(entity, PaperDTO.class);
			 List<PublisherDTO> listAllDto = new ArrayList<PublisherDTO>();
				for (Publisher entityPublisher : entity.getPublisher()) {
					PublisherDTO dtoPublisher = mapper.map(entityPublisher, PublisherDTO.class);					
					listAllDto.add(dtoPublisher);
				}
	    		result.put("status",  status.value());
	            result.put("message", message);
	            result.put("data", paperDto);
	            result.put("list publisher", listAllDto);
	            
	            return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to get Paper");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}		
	}
	
	@PutMapping("/update")
	public ResponseEntity<Object> updatePaper(@Valid @RequestParam(name = "id") Long paperID, @Valid @RequestBody Paper paperUpdate) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Update Paper Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Paper paper = paperRepo.findById(paperID).orElse(null);
			
			 if (paper == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "paper id : "+ paperID + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
			 paper.setQualityName(paperUpdate.getQualityName());
			 paper.setPaperPrice(paperUpdate.getPaperPrice());
			 paperRepo.save(paper);
			 PaperDTO paperDto = mapper.map(paper, PaperDTO.class);
            
            result.put("status",  status.value());
            result.put("message", message);
            result.put("data", paperDto);
            return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to update Paper");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
		
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deletePaper(@Valid @PathVariable(value = "id") Long paperID) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Delete Paper Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Paper paper = paperRepo.findById(paperID).orElse(null);
			
			 if (paper == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Paper id : "+ paperID + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
	            
			paperRepo.delete(paper);
            result.put("status",  status.value());
            result.put("message", message);

            return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to delete Paper");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
	}
}
