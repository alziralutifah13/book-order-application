package com.example.BookOrderAplication.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.influx.InfluxDbOkHttpClientBuilderProvider;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderAplication.DTO.OrderBookDetailsDTO;
import com.example.BookOrderAplication.model.Book;
import com.example.BookOrderAplication.model.Order;
import com.example.BookOrderAplication.model.OrderBookDetails;
import com.example.BookOrderAplication.model.OrderDetailsKey;
import com.example.BookOrderAplication.repository.BookRepository;
import com.example.BookOrderAplication.repository.OrderBookDetailsRepository;
import com.example.BookOrderAplication.repository.OrderRepository;

@RestController
@RequestMapping("/api/order-book")
public class OrderBookDetailsController {
	@Autowired
	OrderBookDetailsRepository orderBookDetailsRepo;
	@Autowired
	OrderRepository orderRepo;
	@Autowired
	BookRepository bookRepo;
	
	ModelMapper mapper = new ModelMapper();
	
	@PostMapping("/create")
	public ResponseEntity<Object> createOrderBookDetails(@Valid @RequestBody OrderBookDetails body) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Create OrderBookDetails Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {	
			Book entityBook = bookRepo.findById(body.getOrderKey().getBookID()).orElse(null);
			Order entityOrder = orderRepo.findById(body.getOrderKey().getOrderID()).orElse(null);
			
			if (entityOrder == null || entityBook == null) {
				message = "ERROR, ID tidak Ditemukan!!!";
				status = HttpStatus.NOT_FOUND;
				
				result.put("status", status);
				result.put("message", message);
				
				return ResponseEntity.status(status).body(result);
			}
		
            body.setTax(calculateTax(body, entityBook.getPrice()));

            OrderBookDetails entity = orderBookDetailsRepo.save(body);
            body.setOrder(entityOrder);
            body.setBook(entityBook);
            body.setOrderKey(entity.getOrderKey());
            
            OrderBookDetailsDTO dto = mapper.map(body, OrderBookDetailsDTO.class);
			
			result.put("message", message);
			result.put("status", status.value());
			result.put("data", dto);
			
			return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to create OrderBookDetails");
			result.put("error", e);
			
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}
	}
	
	@GetMapping("/getall")
	public ResponseEntity<Object> getAllOrderBook() {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find All OrderBookDetails Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			List<OrderBookDetails> listAll = orderBookDetailsRepo.findAll();
			List<OrderBookDetailsDTO> listDTO = new ArrayList<OrderBookDetailsDTO>();
			
			//mapping data dari Entity ke DTO
			for (OrderBookDetails entity : listAll) {
				OrderBookDetailsDTO dto = mapper.map(entity, OrderBookDetailsDTO.class);
		
				listDTO.add(dto);
			}
			
			
			if (listDTO.isEmpty()) {
				status = HttpStatus.NOT_FOUND;
				message = "OrderBookDetails is Empty";
				
				result.put("status", status);
				result.put("message", message);
				result.put("data", listDTO);
				
				return ResponseEntity.status(status).body(result);
			}
			
			result.put("status", status.value());
			result.put("message", message);
			result.put("data", listDTO);
			
			return ResponseEntity.status(status).body(result);
			
		} catch (Exception e) {
			result.put("message", "Failed to get OrderBookDetails");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}		
	}
	
	@GetMapping("/get")
	public ResponseEntity<Object> getById(@Valid @RequestBody OrderDetailsKey orderKey) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find OrderBookDetails by Id Succes!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			OrderBookDetails entity = orderBookDetailsRepo.findById(orderKey).orElse(null);
			
			 if (entity == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "OrderBookDetails id : "+ orderKey + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
			 	OrderBookDetailsDTO dto = mapper.map(entity, OrderBookDetailsDTO.class);
				 
				result.put("status",  status.value());
	            result.put("message", message);
	            result.put("data", dto);
	            
				return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to get OrderBookDetails");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateOrderBook(@Valid @PathVariable(value = "id") OrderDetailsKey orderKey, @Valid @RequestBody OrderBookDetails update) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Update OrderBookDetails Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			OrderBookDetails entity = orderBookDetailsRepo.findById(orderKey).orElse(null);
			
			 if (entity == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "OrderBookDetails id : "+ orderKey + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
			 entity.setOrderKey(update.getOrderKey());
			 entity.setQuantity(update.getQuantity());
			 entity.setDiscount(update.getDiscount());
			 entity.setBook(update.getBook());
			 entity.setOrder(update.getOrder());
			 entity.setTax(update.getTax());
			 
			 orderBookDetailsRepo.save(entity);
			 OrderBookDetailsDTO dto = mapper.map(entity, OrderBookDetailsDTO.class);
			 
            result.put("status",  status.value());
            result.put("message", message);
            result.put("data", dto);
            return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to update OrderBookDetails");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
		
	}
	
	@DeleteMapping("/delete/{bookId}/{orderId}")
	public ResponseEntity<Object> deleteOrderBookDetails(@Valid @PathVariable(name = "bookId") Long bookId,
	        @PathVariable(name = "orderId") Long orderId) {
	    HashMap<String, Object> result = new HashMap<>();
	    String message = "Delete OrderBookDetails Success!!";
	    HttpStatus status = HttpStatus.OK;

	    try {
	        OrderDetailsKey orderKey = new OrderDetailsKey(bookId, orderId);
	        Optional<OrderBookDetails> optional = orderBookDetailsRepo.findById(orderKey);

	        if (optional.isPresent()) {
	            orderBookDetailsRepo.delete(optional.get());
	            result.put("status", status.value());
	            result.put("message", message);
	            return ResponseEntity.status(status).body(result);
	        } else {
	            status = HttpStatus.NOT_FOUND;
	            message = "OrderBookDetails id not found!!!";
	            result.put("status", status);
	            result.put("message", message);
	            return ResponseEntity.status(status).body(result);
	        }

	    } catch (Exception e) {
	        result.put("message", "Failed to delete orderBookDetails");
	        result.put("error", e.getMessage());
	        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
	    }
	}

	
	public BigDecimal calculateTax(OrderBookDetails orderDetails, BigDecimal price) {
		BigDecimal tax = new BigDecimal(0);
		BigDecimal taxPercentage = new BigDecimal(0.05);
		 tax = tax.add(price.multiply(new BigDecimal(orderDetails.getQuantity())).multiply(taxPercentage));
		return tax;
	}
	
	public void calculateDiscount(List<OrderBookDetails> listData) {
		boolean isDiscount = false;
		
		if (listData.size() >= 3) {
			isDiscount = true;
		}
		
		if (isDiscount) {
			for (OrderBookDetails listOrder : listData) {
				listOrder.setDiscount(listOrder.getBook().getPrice().multiply(new BigDecimal(listOrder.getQuantity()*0.1)));
			}
		}else {
			for (OrderBookDetails listOrder : listData) {
				listOrder.setDiscount(new BigDecimal(0));
			}
		}
	}
}
