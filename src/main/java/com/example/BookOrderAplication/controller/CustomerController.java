package com.example.BookOrderAplication.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderAplication.DTO.CustomerDTO;
import com.example.BookOrderAplication.model.Customer;
import com.example.BookOrderAplication.repository.CustomerRepository;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {
	
	@Autowired
	CustomerRepository customerRepo;
	
	ModelMapper mapper = new ModelMapper();
	
	@PostMapping("/create")
	public ResponseEntity<Object> createCutomer(@Valid @RequestBody Customer body) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Create Customer Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			customerRepo.save(body);
			CustomerDTO entity = mapper.map(body, CustomerDTO.class);
			
			result.put("message", message);
			result.put("status", status.value());
			result.put("data", entity);
			
			return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to create Customer");
			result.put("error", e);
			
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}
	}
	
	@GetMapping("/getall")
	public ResponseEntity<Object> getAllCustomer() {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find All Customer Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			List<Customer> listAllCustomer = customerRepo.findAll();
			List<CustomerDTO> listCustomerDTO = new ArrayList<CustomerDTO>();
			
			for (Customer entityCustomer : listAllCustomer) {
				CustomerDTO dtoCustomer = mapper.map(entityCustomer, CustomerDTO.class);
				
				listCustomerDTO.add(dtoCustomer);
			}
			
			if (listCustomerDTO.isEmpty()) {
				status = HttpStatus.NOT_FOUND;
				message = "Customer is Empty";
				
				result.put("status", status);
				result.put("message", message);
				result.put("data", listCustomerDTO);
				
				return ResponseEntity.status(status).body(result);
			}
			
			result.put("status", status.value());
			result.put("message", message);
			result.put("data", listCustomerDTO);
			
			return ResponseEntity.status(status).body(result);
			
		} catch (Exception e) {
			result.put("message", "Failed to get Customer");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}		
	}
	
	@GetMapping("/get")
	public ResponseEntity<Object> getCustomerById(@Valid @RequestParam(name = "id") Long customerId) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find Customer by Id Succes!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Customer entityCustomer = customerRepo.findById(customerId).orElse(null);
			
			 if (entityCustomer == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Customer id : "+ customerId + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
			 	CustomerDTO dtoCustomer = mapper.map(entityCustomer, CustomerDTO.class);
				
	    		result.put("status",  status.value());
	            result.put("message", message);
	            result.put("data", dtoCustomer);
	            
	            return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to get Customer");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}		
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateCustomer(@Valid @PathVariable(value = "id") Long customerId, @Valid @RequestBody Customer customerUpdate) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Update Customer Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Customer customer = customerRepo.findById(customerId).orElse(null);
			
			 if (customer == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Customer id : "+ customerId + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
			 customer.setCustomerName(customerUpdate.getCustomerName());
			 customer.setCountry(customerUpdate.getCountry());
			 customer.setAddress(customerUpdate.getAddress());
			 customer.setPhoneNumber(customerUpdate.getPhoneNumber());
			 customer.setPostalCode(customerUpdate.getPostalCode());
			 customer.setEmail(customerUpdate.getEmail());
			 customerRepo.save(customer);   
			 
			 CustomerDTO dtoCustomer = mapper.map(customer, CustomerDTO.class);
			            
			 result.put("status",  status.value());
	         result.put("message", message);
	         result.put("data", dtoCustomer);
	         
             return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to update Customer");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
		
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteCustomer(@Valid @PathVariable(value = "id") Long customerId) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Delete Customer Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Customer customer = customerRepo.findById(customerId).orElse(null);
			
			 if (customer == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Customer id : "+ customerId + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
	            
            customerRepo.delete(customer);
            result.put("status",  status.value());
            result.put("message", message);

            return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to delete Customer");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
	}
	
}
