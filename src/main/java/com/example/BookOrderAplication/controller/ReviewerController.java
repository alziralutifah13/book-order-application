package com.example.BookOrderAplication.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderAplication.DTO.ReviewerDTO;
import com.example.BookOrderAplication.model.Reviewer;
import com.example.BookOrderAplication.repository.ReviewerRepository;

@RestController
@RequestMapping("/api/reviewer")
public class ReviewerController {
	
	@Autowired
	ReviewerRepository reviewerRepo;
	
	ModelMapper mapper = new ModelMapper();
	
	@PostMapping("/create")
	public ResponseEntity<Object> createReviewer(@Valid @RequestBody Reviewer review) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Create Reviewer Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Reviewer save = reviewerRepo.save(review);
			ReviewerDTO entityReviewer = mapper.map(review, ReviewerDTO.class);
			
			result.put("status", status);
			result.put("message", message);
			result.put("data", entityReviewer);
			result.put("data", save);
			return ResponseEntity.status(status).body(result);
			
		} catch (Exception e) {
			result.put("message", "Failed to create Reviewer");
			result.put("error", e);
			
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}
	}
	
	@GetMapping("/getall")
	public ResponseEntity<Object> getAllReviewer() {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find All Reviewer Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			List<Reviewer> listAll = reviewerRepo.findAll();
			List<ReviewerDTO> listDTO = new ArrayList<ReviewerDTO>();
			
			for (Reviewer entity : listAll) {
				ReviewerDTO dtoReviewer = mapper.map(entity, ReviewerDTO.class);
				
				listDTO.add(dtoReviewer);
			}
			
			if (listDTO.isEmpty()) {
				status = HttpStatus.NOT_FOUND;
				message = "Reviewer is Empty";
				
				result.put("status", status);
				result.put("message", message);
				result.put("data", listDTO);
				
				return ResponseEntity.status(status).body(result);
			}
			
			result.put("status", status.value());
			result.put("message", message);
			result.put("data", listDTO);
			
			return ResponseEntity.status(status).body(result);
			
		} catch (Exception e) {
			result.put("message", "Failed to get Reviewer");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}		
	}
	
	@GetMapping("/get")
	public ResponseEntity<Object> getReviewerById(@Valid @RequestParam(name = "id") Long reviewerID) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find Reviewer by Id Succes!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Reviewer entity = reviewerRepo.findById(reviewerID).orElse(null);
			
			 if (entity == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Reviewer id : "+ reviewerID + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
			 	ReviewerDTO dtoReviewer = mapper.map(entity, ReviewerDTO.class);
					
				result.put("status", status);
				result.put("message", message);
				result.put("data", dtoReviewer);
	            
	            return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to get Reviewer");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}		
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateReviewer(@Valid @PathVariable(value = "id") Long reviewerID, @Valid @RequestBody Reviewer reviewerUpdate) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Update Reviewer Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Reviewer reviewer = reviewerRepo.findById(reviewerID).orElse(null);
			
			 if (reviewer == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Reviewer id : "+ reviewerID + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
			 reviewer.setReviewName(reviewerUpdate.getReviewName());
			 reviewer.setCountry(reviewerUpdate.getCountry());
			 reviewer.setVerified(reviewerUpdate.getVerified());
			 reviewerRepo.save(reviewer);
			 ReviewerDTO dtoReviewer = mapper.map(reviewer, ReviewerDTO.class);
            
			 result.put("status",  status.value());
	         result.put("message", message);
	         result.put("data", dtoReviewer);

             return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to update Reviewer");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
		
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteReviewer(@Valid @PathVariable(value = "id") Long revieweID) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Delete Reviewer Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Reviewer reviewer = reviewerRepo.findById(revieweID).orElse(null);
			
			 if (reviewer == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Reviewer id : "+ revieweID + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
	            
            reviewerRepo.delete(reviewer);
            result.put("status",  status.value());
            result.put("message", message);

            return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to delete Reviewer");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
	}
	
}
