package com.example.BookOrderAplication.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderAplication.DTO.BookDTO;
import com.example.BookOrderAplication.model.Author;
import com.example.BookOrderAplication.model.Book;
import com.example.BookOrderAplication.model.Paper;
import com.example.BookOrderAplication.model.Publisher;
import com.example.BookOrderAplication.repository.AuthorRepository;
import com.example.BookOrderAplication.repository.BookRepository;
import com.example.BookOrderAplication.repository.PublisherRepository;

@RestController
@RequestMapping("/api/book")
public class BookController {
	@Autowired
	BookRepository bookRepo;
	@Autowired
	AuthorRepository authorRepo;
	@Autowired
	PublisherRepository publisherRepo;
	
	ModelMapper mapper = new ModelMapper();
	
	@PostMapping("/create")
	public ResponseEntity<Object> createBook(@Valid @RequestBody Book book) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Create Book Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Author entityAuthor = authorRepo.findById(book.getAuthor().getAuthorId()).orElse(null);
			Publisher entityPublisher = publisherRepo.findById(book.getPublisher().getPublisherID()).orElse(null);
			
			if (entityAuthor == null || entityPublisher == null) {
				message = "ERROR, ID tidak Ditemukan!!!";
				status = HttpStatus.NOT_FOUND;
				
				result.put("status", status);
				result.put("message", message);
				
				return ResponseEntity.status(status).body(result);
			}
			BigDecimal pricePercentage = new BigDecimal("1.5");
			BigDecimal bookPrice = calculatePrice(entityPublisher, pricePercentage);
			book.setPrice(bookPrice);
			Book saveBook = bookRepo.save(book);
			BookDTO entityBook = mapper.map(saveBook, BookDTO.class);
			
			result.put("message", message);
			result.put("status", status.value());
			result.put("data", entityBook);
			
			return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to create Author");
			result.put("error", e);
			
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}
	}

	@GetMapping("/getall")
	public ResponseEntity<Object> getAllBook() {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find All Book Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			List<Book> listAllBook = bookRepo.findAll();
			List<BookDTO> listBookDTO = new ArrayList<BookDTO>();
			
			//mapping data dari Entity ke DTO
			for (Book entityBook : listAllBook) {
				BookDTO dtoBook = mapper.map(entityBook, BookDTO.class);
		
				listBookDTO.add(dtoBook);
			}
			
			
			if (listBookDTO.isEmpty()) {
				status = HttpStatus.NOT_FOUND;
				message = "Book is Empty";
				
				result.put("status", status);
				result.put("message", message);
				result.put("data", listBookDTO);
				
				return ResponseEntity.status(status).body(result);
			}
			
			result.put("status", status.value());
			result.put("message", message);
			result.put("data", listBookDTO);
			
			return ResponseEntity.status(status).body(result);
			
		} catch (Exception e) {
			result.put("message", "Failed to get Book");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}		
	}
	
	@GetMapping("/get")
	public ResponseEntity<Object> getBookById(@Valid @RequestParam(name = "id") Long bookID) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find Book by Id Succes!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Book entityBook = bookRepo.findById(bookID).orElse(null);
			
			 if (entityBook == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Book id : "+ bookID + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
			 	BookDTO dtoBook = mapper.map(entityBook, BookDTO.class);
				 
				result.put("status",  status.value());
	            result.put("message", message);
	            result.put("data", dtoBook);
	            
				return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to get Book");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateBook(@Valid @PathVariable(value = "id") Long bookID, @Valid @RequestBody Book bookUpdate) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Update Book Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Book book = bookRepo.findById(bookID).orElse(null);
			
			 if (book == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Book id : "+ bookID + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
			 book.setTitle(bookUpdate.getTitle());
			 book.setReleaseDate(bookUpdate.getReleaseDate());
			 bookRepo.save(book);
			 BookDTO dtoBook = mapper.map(book, BookDTO.class);
			 
            result.put("status",  status.value());
            result.put("message", message);

            return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to update Book");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
		
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteBook(@Valid @PathVariable(value = "id") Long bookID) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Delete Book Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Book book = bookRepo.findById(bookID).orElse(null);
			
			 if (book == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Author id : "+ bookID + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
	            
			bookRepo.delete(book);
            result.put("status",  status.value());
            result.put("message", message);

            return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to delete Book");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
	}
	
	public BigDecimal calculatePrice(Publisher publisher, BigDecimal price) {
		Paper paper = publisher.getPaper();
	    BigDecimal paperPrice = paper.getPaperPrice();
	    BigDecimal bookPrice = paperPrice.multiply(new BigDecimal("1.5"));
	    return bookPrice;
	}
	
}
