package com.example.BookOrderAplication.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderAplication.DTO.OrderBookDetailsDTO;
import com.example.BookOrderAplication.DTO.OrderDTO;
import com.example.BookOrderAplication.model.Book;
import com.example.BookOrderAplication.model.Customer;
import com.example.BookOrderAplication.model.Order;
import com.example.BookOrderAplication.model.OrderBookDetails;
import com.example.BookOrderAplication.model.OrderDetailsKey;
import com.example.BookOrderAplication.repository.BookRepository;
import com.example.BookOrderAplication.repository.CustomerRepository;
import com.example.BookOrderAplication.repository.OrderBookDetailsRepository;
import com.example.BookOrderAplication.repository.OrderRepository;

@RestController
@RequestMapping("/api")
public class CalcultateController {
	@Autowired
	OrderBookDetailsRepository orderBookDetailsRepo;
	@Autowired
	OrderRepository orderRepo;
	@Autowired
	BookRepository bookRepo;
	@Autowired
	CustomerRepository customerRepo;
	
	ModelMapper mapper = new ModelMapper();
	
	@PostMapping("/calculate-discount")
	public ResponseEntity<Object> calculateDiscount(@Valid @RequestParam("orderID") Long orderID) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Calculate Success!!";
		HttpStatus status = HttpStatus.OK;
			
		Order entityOrder = orderRepo.findById(orderID).orElse(null);
		
			try {
				if (entityOrder == null) {
					message = "ERROR, ID tidak Ditemukan!!!";
					status = HttpStatus.NOT_FOUND;
					
					result.put("status", status);
					result.put("message", message);
					
					return ResponseEntity.status(status).body(result);
				}
			
				List<OrderBookDetails> listOrderDetails = entityOrder.getOrderBookDetails();
				calculateDiscount(listOrderDetails);
				entityOrder.setOrderBookDetails(listOrderDetails);
				
				Order entity = orderRepo.save(entityOrder);
		
				result.put("message", message);
				result.put("status", status.value());
				result.put("data", entity);
				
				return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to create OrderBookDetails");
			result.put("error", e);
			
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}
	}
	
	@PostMapping("/calculate-all-discount")
	public ResponseEntity<Object> calculateAllDiscount() {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Calculate Success!!";
		HttpStatus status = HttpStatus.OK;
			
		List<Order> listOrder = orderRepo.findAll();
		
			try {
				if (listOrder.isEmpty()) {
					status = HttpStatus.NOT_FOUND;
					message = "OrderBookDetails is Empty";
					
					result.put("status", status);
					result.put("message", message);
					result.put("data", listOrder);
					
					return ResponseEntity.status(status).body(result);
				}
			
				for (Order data : listOrder) {
					List<OrderBookDetails> listOrderDetails = data.getOrderBookDetails();
					calculateDiscount(listOrderDetails);
					data.setOrderBookDetails(listOrderDetails);
					
					for (OrderBookDetails tax : listOrderDetails) {
						tax.setTax(calculateTax(tax));
					}
					
					data.setOrderBookDetails(listOrderDetails);
					
					orderRepo.save(data);
				}
		
				result.put("message", message);
				result.put("status", status.value());
				
				return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to Calculate");
			result.put("error", e);
			
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}
	}
	
	@PostMapping("/calculate-all-totalorder")
	public ResponseEntity<Object> calculateAllTotalOrder() {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Calculate Success!!";
		HttpStatus status = HttpStatus.OK;
			
		List<Order> listOrder = orderRepo.findAll();
		
			try {
				if (listOrder.isEmpty()) {
					status = HttpStatus.NOT_FOUND;
					message = "OrderBookDetails is Empty";
					
					result.put("status", status);
					result.put("message", message);
					result.put("data", listOrder);
					
					return ResponseEntity.status(status).body(result);
				}
			
				for (Order data : listOrder) {
					List<OrderBookDetails> listOrderDetails = data.getOrderBookDetails();
					calculateTotalOrder(listOrderDetails);
					data.setOrderBookDetails(listOrderDetails);
					
					orderRepo.save(data);
				}
		
				result.put("message", message);
				result.put("status", status.value());
				
				return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to Calculate");
			result.put("error", e);
			
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}
	}
	
	@PostMapping("/calculate-total-order")
	public ResponseEntity<Object> calculateTotalOrder(@Valid @RequestParam("orderID") Long orderID) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Calculate Success!!";
		HttpStatus status = HttpStatus.OK;
			
		Order entityOrder = orderRepo.findById(orderID).orElse(null);
		
			try {
				if (entityOrder == null) {
					message = "ERROR, ID tidak Ditemukan!!!";
					status = HttpStatus.NOT_FOUND;
					
					result.put("status", status);
					result.put("message", message);
					
					return ResponseEntity.status(status).body(result);
				}
			
				List<OrderBookDetails> listOrderDetails = entityOrder.getOrderBookDetails();
				entityOrder.setTotalOrder(calculateTotalOrder(listOrderDetails));
				
				Order entity = orderRepo.save(entityOrder);
				OrderDTO dto = mapper.map(entity, OrderDTO.class);
				
				result.put("message", message);
				result.put("status", status.value());
				result.put("data", dto);
				
				return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to Calculate");
			result.put("error", e);
			
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}
	}
	
	@PostMapping("/calculate-discount-tax-and-total-order")
	public ResponseEntity<Object> calculateDiscountTaxAndTotalOrder(@RequestBody Order order) {
	    HashMap<String, Object> result = new HashMap<>();
	    String message = "Calculate Success!!";
	    HttpStatus status = HttpStatus.OK;
	   
	    try {
	    	Customer entityCustomer = customerRepo.findById(order.getCustomer().getCustomerId()).orElse(null);
	    	
	    	if (entityCustomer == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Customer id not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }

	        for (OrderBookDetails data : order.getOrderBookDetails()) {
	        	Book entityBook = bookRepo.findById(data.getOrderKey().getBookID()).orElse(null);
	        	
	        	if (entityBook == null) {
	    			status = HttpStatus.NOT_FOUND;
	                message = "Book id not found!!!";
	                
	                result.put("status", status);
	                result.put("message", message);

	                return ResponseEntity.status(status).body(result);
		         }
	        }
	        
	        
	        List<OrderBookDetails> orderDetails = order.getOrderBookDetails();
	        		
        	Order savedOrder = orderRepo.save(order);
        	for (OrderBookDetails orderBookDetails : orderDetails) {
        	    orderBookDetails.getOrderKey().setOrderID(order.getOrderID());
        	    
        	}
      	
        	
        	orderDetails = calculateDiscountt(orderDetails);
        	orderDetails = calculateTax(orderDetails);
        	BigDecimal totalOrder = new BigDecimal(0);
        	
            for (OrderBookDetails orderBookDetails : orderDetails) {
            	Book book = bookRepo.findById(orderBookDetails.getOrderKey().getBookID()).orElse(null);

//                BigDecimal tax = calculateTax(orderBookDetails, book.getPrice());
//                orderBookDetails.setTax(tax);
//                System.out.println(tax);
                
                System.out.println(orderBookDetails.getDiscount());
                System.out.println(orderBookDetails.getTax());
                
//                totalOrder = totalOrder.add(book.getPrice().multiply(BigDecimal.valueOf(orderBookDetails.getQuantity())).subtract(orderBookDetails.getDiscount()).add(tax));
                		
                orderBookDetailsRepo.save(orderBookDetails);
                System.out.println(orderBookDetails.getOrderKey().getOrderID());
               
            }
          
//             calculate total order
            BigDecimal totalOrderr = calculateTotalOrder(orderDetails);

            // update total order
            savedOrder.setTotalOrder(totalOrderr);
            orderRepo.save(savedOrder);
            
            
	        result.put("message", message);
	        result.put("status", status.value());
//	        result.put("data", orderDTO);
	        return ResponseEntity.status(status).body(result);
	    } catch (Exception e) {
	        result.put("message", "Failed to calculate discount, tax, and total order");
	        result.put("error", e);

	        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
	    }
	}

	
	public BigDecimal calculateTax(OrderBookDetails orderDetails) {
		BigDecimal tax = new BigDecimal(0);
		BigDecimal taxPercentage = new BigDecimal(0.05);
		tax = tax.add(orderDetails.getBook().getPrice().multiply(new BigDecimal(orderDetails.getQuantity())).multiply(taxPercentage));
		return tax;
	}
	
	public List<OrderBookDetails> calculateTax(List<OrderBookDetails> listData) {
		BigDecimal taxPercentage = new BigDecimal(0.05);
		for (OrderBookDetails orderBookDetails : listData) {
			Book book = bookRepo.findById(orderBookDetails.getOrderKey().getBookID()).get();
			BigDecimal orderTax = book.getPrice().multiply((new BigDecimal(orderBookDetails.getQuantity())).multiply(taxPercentage));
			orderBookDetails.setTax(orderTax);
		}
		
		return listData;
	}
	
	public List<OrderBookDetails> calculateDiscountt(List<OrderBookDetails> listData) {
		BigDecimal discount = new BigDecimal(0);
		boolean isDiscount = false;
		
		if (listData.size() >= 3) {
			isDiscount = true;
		}
		
		if (isDiscount) {
			for (OrderBookDetails listOrder : listData) {
				Book book = bookRepo.findById(listOrder.getOrderKey().getBookID()).get();
				 BigDecimal orderDiscount = book.getPrice().multiply(new BigDecimal(listOrder.getQuantity()*0.1));
		         discount = discount.add(orderDiscount);
		         listOrder.setDiscount(orderDiscount);
			}
		}else {
			for (OrderBookDetails listOrder : listData) {
				listOrder.setDiscount(new BigDecimal(0));
			}
		}
		return listData;
	}
	
	public void calculateDiscount(List<OrderBookDetails> listData) {
		boolean isDiscount = false;
		
		if (listData.size() >= 3) {
			isDiscount = true;
		}
		
		if (isDiscount) {
			for (OrderBookDetails listOrder : listData) {
				listOrder.setDiscount(listOrder.getBook().getPrice().multiply(new BigDecimal(listOrder.getQuantity()*0.1)));
			}
		}else {
			for (OrderBookDetails listOrder : listData) {
				listOrder.setDiscount(new BigDecimal(0));
			}
		}
	}
	
	public BigDecimal calculateTotalOrder(List<OrderBookDetails> orderDetails) {
		BigDecimal totalOrder = new BigDecimal(0);
		for (OrderBookDetails data : orderDetails) {
			Book book = bookRepo.findById(data.getOrderKey().getBookID()).get();
			totalOrder = totalOrder.add(book.getPrice().multiply(new BigDecimal(data.getQuantity()))).subtract(data.getDiscount()).add(data.getTax());
		}
		return totalOrder;	
	}
}
