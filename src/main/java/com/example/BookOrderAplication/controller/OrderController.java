package com.example.BookOrderAplication.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderAplication.DTO.OrderDTO;
import com.example.BookOrderAplication.model.Customer;
import com.example.BookOrderAplication.model.Order;
import com.example.BookOrderAplication.model.OrderBookDetails;
import com.example.BookOrderAplication.repository.CustomerRepository;
import com.example.BookOrderAplication.repository.OrderRepository;

@RestController
@RequestMapping("/api/order")
public class OrderController {
	@Autowired
	OrderRepository orderRepo;
	@Autowired
	CustomerRepository customerRepo;
	
	ModelMapper mapper =  new ModelMapper();
	
	@PostMapping("/create")
	public ResponseEntity<Object> createOrder(@Valid @RequestBody OrderDTO order) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Create Book Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Customer entityCustomer = customerRepo.findById(order.getCustomer().getCustomerId()).orElse(null);
			
			if (entityCustomer == null) {
				message = "ERROR, Customer ID tidak Ditemukan!!!";
				status = HttpStatus.NOT_FOUND;
				
				result.put("status", status);
				result.put("message", message);
				
				return ResponseEntity.status(status).body(result);
			}
			
			Order entityOrder = mapper.map(order, Order.class);
			entityOrder = orderRepo.save(entityOrder);
		
			result.put("message", message);
			result.put("status", status.value());
			result.put("data", entityOrder);
			
			return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to create Author");
			result.put("error", e);
			
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}
	}
	
	@GetMapping("/getall")
	public ResponseEntity<Object> getAllOrder() {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find All Order Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			List<Order> listAll = orderRepo.findAll();
			List<OrderDTO> listDTO = new ArrayList<OrderDTO>();
			
			//mapping data dari Entity ke DTO
			for (Order entity : listAll) {
				OrderDTO dto = mapper.map(entity, OrderDTO.class);
		
				listDTO.add(dto);
			}
			
			
			if (listDTO.isEmpty()) {
				status = HttpStatus.NOT_FOUND;
				message = "Order is Empty";
				
				result.put("status", status);
				result.put("message", message);
				result.put("data", listDTO);
				
				return ResponseEntity.status(status).body(result);
			}
			
			result.put("status", status.value());
			result.put("message", message);
			result.put("data", listDTO);
			
			return ResponseEntity.status(status).body(result);
			
		} catch (Exception e) {
			result.put("message", "Failed to get Order");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}		
	}
	
	@GetMapping("/get")
	public ResponseEntity<Object> getOrderById(@Valid @RequestParam(name = "id") Long orderID) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find Order by Id Succes!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Order entity = orderRepo.findById(orderID).orElse(null);
			
			 if (entity == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Order id : "+ orderID + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
			 	OrderDTO dto = mapper.map(entity, OrderDTO.class);
				 
				result.put("status",  status.value());
	            result.put("message", message);
	            result.put("data", dto);
	            
				return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to get Order");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateOrder(@Valid @PathVariable(value = "id") Long orderID, @Valid @RequestBody Order orderUpdate) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Update Order Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Order order = orderRepo.findById(orderID).orElse(null);
			
			 if (order == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Order id : "+ orderID + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
	         order.setOrderID(orderUpdate.getOrderID());
	         order.setOrderDate(orderUpdate.getOrderDate());
	         order.setTotalOrder(orderUpdate.getTotalOrder());
			 orderRepo.save(order);
			 OrderDTO dto = mapper.map(order, OrderDTO.class);
			 
            result.put("status",  status.value());
            result.put("message", message);
            result.put("data", dto);
            return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to update Order");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
		
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteOrder(@Valid @PathVariable(value = "id") Long orderID) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Delete Book Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Order order = orderRepo.findById(orderID).orElse(null);
			
			 if (order == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Order id : "+ orderID + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
	            
			orderRepo.delete(order);
            result.put("status",  status);
            result.put("message", message);

            return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to delete Order");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
	}
	
	public BigDecimal calculateTotalOrder(List<OrderBookDetails> orderDetails) {
		BigDecimal totalOrder = new BigDecimal(0);
		for (OrderBookDetails data : orderDetails) {
			totalOrder = totalOrder.add(data.getBook().getPrice().multiply(new BigDecimal(data.getQuantity()))).subtract(data.getDiscount()).add(data.getTax());
		}
		return totalOrder;	
	}
}
