package com.example.BookOrderAplication.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderAplication.DTO.BookDTO;
import com.example.BookOrderAplication.DTO.PublisherDTO;
import com.example.BookOrderAplication.model.Book;
import com.example.BookOrderAplication.model.Paper;
import com.example.BookOrderAplication.model.Publisher;
import com.example.BookOrderAplication.repository.PaperRepository;
import com.example.BookOrderAplication.repository.PublisherRepository;

@RestController
@RequestMapping("/api/publisher")
public class PublisherController {
	@Autowired
	PublisherRepository publisherRepo;
	
	@Autowired
	PaperRepository paperRepo;
	
	ModelMapper mapper = new ModelMapper();
	
	@PostMapping("/create")
	public ResponseEntity<Object> createPublisher(@Valid @RequestBody PublisherDTO body) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Create Publisher Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Paper entityPaper = paperRepo.findById(body.getPaper().getPaperID()).orElse(null);
			
			if (entityPaper == null) {
				message = "ERROR, Paper ID tidak Ditemukan!!!";
				status = HttpStatus.NOT_FOUND;
				
				result.put("status", status);
				result.put("message", message);
				
				return ResponseEntity.status(status).body(result);
			}
			
			Publisher entity = mapper.map(body, Publisher.class);
			
			//lakukan penyimpanan pada repository
			entity = publisherRepo.save(entity);
			
			result.put("message", message);
			result.put("status", status.value());
			result.put("data", entity);
			
			return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to create Publisher");
			result.put("error", e);
			
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}
	}
	
	@GetMapping("/getall")
	public ResponseEntity<Object> getAllPublisher() {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find All Publisher Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			List<Publisher> listAll = publisherRepo.findAll();
			List<PublisherDTO> listDTO = new ArrayList<PublisherDTO>();
			
			for (Publisher entity : listAll) {
				PublisherDTO dtoPublisher = mapper.map(entity, PublisherDTO.class);
				
				listDTO.add(dtoPublisher);
			}
			
			if (listDTO.isEmpty()) {
				status = HttpStatus.NOT_FOUND;
				message = "Publisher is Empty";
				
				result.put("status", status);
				result.put("message", message);
				result.put("data", listDTO);
				
				return ResponseEntity.status(status).body(result);
			}
			
			result.put("status", status.value());
			result.put("message", message);
			result.put("data", listDTO);
			
			return ResponseEntity.status(status).body(result);
			
		} catch (Exception e) {
			result.put("message", "Failed to get Publisher");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}		
	}
	
	@GetMapping("/get")
	public ResponseEntity<Object> getPublisherById(@Valid @RequestParam(name = "id") Long publisherID) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find Publisher by Id Succes!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Publisher entity = publisherRepo.findById(publisherID).orElse(null);
			
			 if (entity == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Publisher id : "+ publisherID + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
			 PublisherDTO dtoPublisher = mapper.map(entity, PublisherDTO.class);
			 List<BookDTO> listBookDTO = new ArrayList<BookDTO>();
				for (Book entityBook : entity.getBooks()) {
					BookDTO dtoBook = mapper.map(entityBook, BookDTO.class);
					
					listBookDTO.add(dtoBook);
				}
				dtoPublisher.setListBooks(listBookDTO);
				
	    		result.put("status",  status.value());
	            result.put("message", message);
	            result.put("data", dtoPublisher);
	            result.put("list publisher", listBookDTO);
	            return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to get Publisher");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}		
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updatePublisher(@Valid @PathVariable(value = "id") Long publisherID, @Valid @RequestBody Publisher publisherUpdate) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Update Publisher Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Publisher publisher = publisherRepo.findById(publisherID).orElse(null);
			
			 if (publisher == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Publisher id : "+ publisherID + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
			publisher.setCompanyName(publisherUpdate.getCompanyName());
            publisher.setCountry(publisherUpdate.getCountry());
            publisher.setPaper(publisherUpdate.getPaper());
            publisherRepo.save(publisher);
			PublisherDTO dtoPublisher = mapper.map(publisher, PublisherDTO.class);
            
            result.put("status",  status.value());
            result.put("message", message);
            result.put("data", dtoPublisher);
            
            return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to update Publisher");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
		
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deletePublisher(@Valid @PathVariable(value = "id") Long publisherID) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Delete Publisher Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Publisher publisher = publisherRepo.findById(publisherID).orElse(null);
			
			 if (publisher == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Author id : "+ publisherID + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
	            
			publisherRepo.delete(publisher);
            result.put("status",  status.value());
            result.put("message", message);

            return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to delete Publisher");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
	}
	
}
