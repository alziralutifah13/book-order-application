package com.example.BookOrderAplication.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "paper")
public class Paper implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long paperID;
	
	@Column(nullable = false)
	private String qualityName;
	
	@Column(nullable = false)
	private BigDecimal paperPrice;

	@OneToMany(mappedBy = "paper", cascade = CascadeType.ALL)
    private List<Publisher> publisher;
	
	public Paper() {
		
	}

	public Paper(Long paperID, String qualityName, BigDecimal paperPrice, List<Publisher> publisher) {
		super();
		this.paperID = paperID;
		this.qualityName = qualityName;
		this.paperPrice = paperPrice;
		this.publisher = publisher;
	}



	public Long getPaperID() {
		return paperID;
	}

	public void setPaperID(Long paperID) {
		this.paperID = paperID;
	}

	

	public String getQualityName() {
		return qualityName;
	}

	public void setQualityName(String qualityName) {
		this.qualityName = qualityName;
	}

	public BigDecimal getPaperPrice() {
		return paperPrice;
	}

	public void setPaperPrice(BigDecimal paperPrice) {
		this.paperPrice = paperPrice;
	}

	public List<Publisher> getPublisher() {
		return publisher;
	}

	public void setPublisher(List<Publisher> publisher) {
		this.publisher = publisher;
	}
	
	
}
