package com.example.BookOrderAplication.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "reviewer")
public class Reviewer implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long reviewID;
	
	@Column(nullable = false)
	private String reviewName;
	
	@Column(nullable = false)
	private String country;
	
	@Column(nullable = false)
	private Boolean verified;
	
	@OneToMany(mappedBy = "book")
	private Set<BookRatings> bookRatings;
	
	
	public Reviewer() {
		
	}

	public Reviewer(Long reviewID, String reviewName, String country, Boolean verified, Set<BookRatings> bookRatings) {
		this.reviewID = reviewID;
		this.reviewName = reviewName;
		this.country = country;
		this.verified = verified;
		this.bookRatings = bookRatings;
	}

	public Long getReviewID() {
		return reviewID;
	}

	public void setReviewID(Long reviewID) {
		this.reviewID = reviewID;
	}

	public String getReviewName() {
		return reviewName;
	}

	public void setReviewName(String reviewName) {
		this.reviewName = reviewName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Boolean getVerified() {
		return verified;
	}

	public void setVerified(Boolean verified) {
		this.verified = verified;
	}

	public Set<BookRatings> getBookRatings() {
		return bookRatings;
	}

	public void setBookRatings(Set<BookRatings> bookRatings) {
		this.bookRatings = bookRatings;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bookRatings == null) ? 0 : bookRatings.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((reviewID == null) ? 0 : reviewID.hashCode());
		result = prime * result + ((reviewName == null) ? 0 : reviewName.hashCode());
		result = prime * result + ((verified == null) ? 0 : verified.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reviewer other = (Reviewer) obj;
		if (bookRatings == null) {
			if (other.bookRatings != null)
				return false;
		} else if (!bookRatings.equals(other.bookRatings))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (reviewID == null) {
			if (other.reviewID != null)
				return false;
		} else if (!reviewID.equals(other.reviewID))
			return false;
		if (reviewName == null) {
			if (other.reviewName != null)
				return false;
		} else if (!reviewName.equals(other.reviewName))
			return false;
		if (verified == null) {
			if (other.verified != null)
				return false;
		} else if (!verified.equals(other.verified))
			return false;
		return true;
	}
	
	
}
