package com.example.BookOrderAplication.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "orders")
public class Order {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_seq_order_id")
	@SequenceGenerator(name = "generator_seq_order_id", sequenceName = "seq_order_id", schema = "public", allocationSize = 1)
	@Column(name = "orderid")
	private Long orderID;
	
	@ManyToOne
    @JoinColumn(name = "customer_id")
	private Customer customer;
	
	@OneToMany(mappedBy = "order")
	private List<OrderBookDetails> orderBookDetails;
	
	@Column(nullable = false, name = "order_date")
	@Temporal(TemporalType.DATE)
	private Date orderDate;
	
	@Column(nullable = false, name = "total_order")
	private BigDecimal totalOrder;

	public Order() {
	}

	public Order(Long orderID, Customer customer, List<OrderBookDetails> orderBookDetails, Date orderDate,
			BigDecimal totalOrder) {
		super();
		this.orderID = orderID;
		this.customer = customer;
		this.orderBookDetails = orderBookDetails;
		this.orderDate = orderDate;
		this.totalOrder = totalOrder;
	}

	public Long getOrderID() {
		return orderID;
	}


	public void setOrderID(Long orderID) {
		this.orderID = orderID;
	}


	public Customer getCustomer() {
		return customer;
	}


	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<OrderBookDetails> getOrderBookDetails() {
		return orderBookDetails;
	}

	public void setOrderBookDetails(List<OrderBookDetails> orderBookDetails) {
		this.orderBookDetails = orderBookDetails;
	}

	public Date getOrderDate() {
		return orderDate;
	}


	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}


	public BigDecimal getTotalOrder() {
		return totalOrder;
	}


	public void setTotalOrder(BigDecimal totalOrder) {
		this.totalOrder = totalOrder;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((customer == null) ? 0 : customer.hashCode());
		result = prime * result + ((orderBookDetails == null) ? 0 : orderBookDetails.hashCode());
		result = prime * result + ((orderDate == null) ? 0 : orderDate.hashCode());
		result = prime * result + ((orderID == null) ? 0 : orderID.hashCode());
		result = prime * result + ((totalOrder == null) ? 0 : totalOrder.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (customer == null) {
			if (other.customer != null)
				return false;
		} else if (!customer.equals(other.customer))
			return false;
		if (orderBookDetails == null) {
			if (other.orderBookDetails != null)
				return false;
		} else if (!orderBookDetails.equals(other.orderBookDetails))
			return false;
		if (orderDate == null) {
			if (other.orderDate != null)
				return false;
		} else if (!orderDate.equals(other.orderDate))
			return false;
		if (orderID == null) {
			if (other.orderID != null)
				return false;
		} else if (!orderID.equals(other.orderID))
			return false;
		if (totalOrder == null) {
			if (other.totalOrder != null)
				return false;
		} else if (!totalOrder.equals(other.totalOrder))
			return false;
		return true;
	}
	
}
