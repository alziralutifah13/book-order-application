package com.example.BookOrderAplication.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "book")
public class Book implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	 private Long bookID;
	
	@Column(nullable = false)
	 private String title;
	
	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	 private Date releaseDate;
	
	@OneToMany(mappedBy = "book")
	private Set<OrderBookDetails> orderBookDetails;
	
	@OneToMany(mappedBy = "book")
	private Set<BookRatings> bookRatings;
	
	@ManyToOne
    @JoinColumn(name = "author_id")
    private Author author;

    @ManyToOne
    @JoinColumn(name = "publisher_id")
    private Publisher publisher;
    
	@Column(nullable = false)
	 private BigDecimal Price;
	

	public Book() {
		
	}

	public Book(Long bookID, String title, Date releaseDate, Set<OrderBookDetails> orderBookDetails,
			Set<BookRatings> bookRatings, Author author, Publisher publisher, BigDecimal price) {
		this.bookID = bookID;
		this.title = title;
		this.releaseDate = releaseDate;
		this.orderBookDetails = orderBookDetails;
		this.bookRatings = bookRatings;
		this.author = author;
		this.publisher = publisher;
		Price = price;
	}



	public Long getBookID() {
		return bookID;
	}

	public void setBookID(Long bookID) {
		this.bookID = bookID;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public Set<OrderBookDetails> getOrderBookDetails() {
		return orderBookDetails;
	}

	public void setOrderBookDetails(Set<OrderBookDetails> orderBookDetails) {
		this.orderBookDetails = orderBookDetails;
	}

	public Set<BookRatings> getBookRatings() {
		return bookRatings;
	}

	public void setBookRatings(Set<BookRatings> bookRatings) {
		this.bookRatings = bookRatings;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public Publisher getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}

	public BigDecimal getPrice() {
		return Price;
	}

	public void setPrice(BigDecimal price) {
		Price = price;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Price == null) ? 0 : Price.hashCode());
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + ((bookID == null) ? 0 : bookID.hashCode());
		result = prime * result + ((bookRatings == null) ? 0 : bookRatings.hashCode());
		result = prime * result + ((orderBookDetails == null) ? 0 : orderBookDetails.hashCode());
		result = prime * result + ((publisher == null) ? 0 : publisher.hashCode());
		result = prime * result + ((releaseDate == null) ? 0 : releaseDate.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (Price == null) {
			if (other.Price != null)
				return false;
		} else if (!Price.equals(other.Price))
			return false;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (bookID == null) {
			if (other.bookID != null)
				return false;
		} else if (!bookID.equals(other.bookID))
			return false;
		if (bookRatings == null) {
			if (other.bookRatings != null)
				return false;
		} else if (!bookRatings.equals(other.bookRatings))
			return false;
		if (orderBookDetails == null) {
			if (other.orderBookDetails != null)
				return false;
		} else if (!orderBookDetails.equals(other.orderBookDetails))
			return false;
		if (publisher == null) {
			if (other.publisher != null)
				return false;
		} else if (!publisher.equals(other.publisher))
			return false;
		if (releaseDate == null) {
			if (other.releaseDate != null)
				return false;
		} else if (!releaseDate.equals(other.releaseDate))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

}
