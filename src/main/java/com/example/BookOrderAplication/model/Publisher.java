package com.example.BookOrderAplication.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "publisher")
public class Publisher implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long publisherID;
	
	@Column(nullable = false)
	private String companyName;
	
	@Column(nullable = false)
	private String country;
	
	@ManyToOne
	@JoinColumn(name = "paper_id")
    private Paper paper;

	@OneToMany(mappedBy = "publisher", cascade = CascadeType.ALL)
    private List<Book> books;
	
	public Publisher() {
		
	}

	public Publisher(Long publisherID, String companyName, String country, Paper paper) {
		super();
		this.publisherID = publisherID;
		this.companyName = companyName;
		this.country = country;
		this.paper = paper;
	}

	public Long getPublisherID() {
		return publisherID;
	}

	public void setPublisherID(Long publisherID) {
		this.publisherID = publisherID;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Paper getPaper() {
		return paper;
	}

	public void setPaper(Paper paper) {
		this.paper = paper;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}
	
}
