package com.example.BookOrderAplication.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "order_book_details")
public class OrderBookDetails {
	
	@EmbeddedId
	private OrderDetailsKey orderKey;
	
	@ManyToOne
    @MapsId("order_id")
    @JoinColumn(name = "order_id")
	private Order order;
	
	@ManyToOne
    @MapsId("book_id")
    @JoinColumn(name = "book_id")
	private Book book;
	
	@Column(name = "quantity", nullable = false)
	private int quantity;
	
	@Column(nullable = false)
	private BigDecimal discount;
	
	@Column(nullable = false)
	private BigDecimal tax;
	
	public OrderBookDetails() {
	}

	public OrderBookDetails(OrderDetailsKey orderKey, Order order, Book book, int quantity, BigDecimal discount,
			BigDecimal tax) {
		super();
		this.orderKey = orderKey;
		this.order = order;
		this.book = book;
		this.quantity = quantity;
		this.discount = discount;
		this.tax = tax;
	}

	public OrderDetailsKey getOrderKey() {
		return orderKey;
	}

	public void setOrderKey(OrderDetailsKey orderKey) {
		this.orderKey = orderKey;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public BigDecimal getTax() {
		return tax;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((book == null) ? 0 : book.hashCode());
		result = prime * result + ((discount == null) ? 0 : discount.hashCode());
		result = prime * result + ((order == null) ? 0 : order.hashCode());
		result = prime * result + ((orderKey == null) ? 0 : orderKey.hashCode());
		result = prime * result + quantity;
		result = prime * result + ((tax == null) ? 0 : tax.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderBookDetails other = (OrderBookDetails) obj;
		if (book == null) {
			if (other.book != null)
				return false;
		} else if (!book.equals(other.book))
			return false;
		if (discount == null) {
			if (other.discount != null)
				return false;
		} else if (!discount.equals(other.discount))
			return false;
		if (order == null) {
			if (other.order != null)
				return false;
		} else if (!order.equals(other.order))
			return false;
		if (orderKey == null) {
			if (other.orderKey != null)
				return false;
		} else if (!orderKey.equals(other.orderKey))
			return false;
		if (quantity != other.quantity)
			return false;
		if (tax == null) {
			if (other.tax != null)
				return false;
		} else if (!tax.equals(other.tax))
			return false;
		return true;
	}
	
	
}
