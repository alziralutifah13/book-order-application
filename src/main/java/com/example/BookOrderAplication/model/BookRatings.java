package com.example.BookOrderAplication.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "book_ratings")
public class BookRatings {
	@Id
	@Column(name = "book_rating_id")
	private Long bookRatingId;
	
	@ManyToOne
    @JoinColumn(name = "book_id")
	private Book book;
	
	@ManyToOne
    @JoinColumn(name = "reviewer_id")
	private Reviewer reviewer;
	
	@Column(name = "rating_score")
	private int ratingScore;

	
	public BookRatings() {
	}

	
	public BookRatings(Long bookRatingId, Book book, Reviewer reviewer, int ratingScore) {
		this.bookRatingId = bookRatingId;
		this.book = book;
		this.reviewer = reviewer;
		this.ratingScore = ratingScore;
	}


	public Long getBookRatingId() {
		return bookRatingId;
	}

	public void setBookRatingId(Long bookRatingId) {
		this.bookRatingId = bookRatingId;
	}

	public Book getBook() {
		return book;
	}


	public void setBook(Book book) {
		this.book = book;
	}


	public Reviewer getReviewer() {
		return reviewer;
	}

	public void setReviewer(Reviewer reviewer) {
		this.reviewer = reviewer;
	}

	public int getRatingScore() {
		return ratingScore;
	}

	public void setRatingScore(int ratingScore) {
		this.ratingScore = ratingScore;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((book == null) ? 0 : book.hashCode());
		result = prime * result + ((bookRatingId == null) ? 0 : bookRatingId.hashCode());
		result = prime * result + ratingScore;
		result = prime * result + ((reviewer == null) ? 0 : reviewer.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BookRatings other = (BookRatings) obj;
		if (book == null) {
			if (other.book != null)
				return false;
		} else if (!book.equals(other.book))
			return false;
		if (bookRatingId == null) {
			if (other.bookRatingId != null)
				return false;
		} else if (!bookRatingId.equals(other.bookRatingId))
			return false;
		if (ratingScore != other.ratingScore)
			return false;
		if (reviewer == null) {
			if (other.reviewer != null)
				return false;
		} else if (!reviewer.equals(other.reviewer))
			return false;
		return true;
	}
	

}
