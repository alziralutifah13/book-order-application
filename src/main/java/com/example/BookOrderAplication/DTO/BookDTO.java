package com.example.BookOrderAplication.DTO;

import java.math.BigDecimal;
import java.util.Date;


public class BookDTO {
	private Long bookID;
	private String title;
	private Date releaseDate;
	private AuthorDTO author;
	private PublisherDTO publisher;
	private BigDecimal Price;
	
	public BookDTO() {
	}

	public BookDTO(Long bookID, String title, Date releaseDate, AuthorDTO author, PublisherDTO publisher,
			BigDecimal price) {
		super();
		this.bookID = bookID;
		this.title = title;
		this.releaseDate = releaseDate;
		this.author = author;
		this.publisher = publisher;
		Price = price;
	}

	public Long getBookID() {
		return bookID;
	}

	public void setBookID(Long bookID) {
		this.bookID = bookID;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public AuthorDTO getAuthor() {
		return author;
	}

	public void setAuthor(AuthorDTO author) {
		this.author = author;
	}

	public PublisherDTO getPublisher() {
		return publisher;
	}

	public void setPublisher(PublisherDTO publisher) {
		this.publisher = publisher;
	}

	public BigDecimal getPrice() {
		return Price;
	}

	public void setPrice(BigDecimal price) {
		Price = price;
	}
	
	
}
