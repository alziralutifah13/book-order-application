package com.example.BookOrderAplication.DTO;

public class ReviewerDTO {
	private Long reviewID;
	private String reviewName;
	private String country;
	private Boolean verified;
	
	
	public ReviewerDTO() {
	}


	public ReviewerDTO(Long reviewID, String reviewName, String country, Boolean verified) {
		super();
		this.reviewID = reviewID;
		this.reviewName = reviewName;
		this.country = country;
		this.verified = verified;
	}


	public Long getReviewID() {
		return reviewID;
	}


	public void setReviewID(Long reviewID) {
		this.reviewID = reviewID;
	}


	public String getReviewName() {
		return reviewName;
	}


	public void setReviewName(String reviewName) {
		this.reviewName = reviewName;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public Boolean getVerified() {
		return verified;
	}


	public void setVerified(Boolean verified) {
		this.verified = verified;
	}
	
	
	
}
