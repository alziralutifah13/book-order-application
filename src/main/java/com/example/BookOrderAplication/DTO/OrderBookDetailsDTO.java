package com.example.BookOrderAplication.DTO;

import java.math.BigDecimal;

import com.example.BookOrderAplication.model.OrderDetailsKey;

public class OrderBookDetailsDTO {
	private OrderDetailsKey orderKey;
	private OrderDTO order;
	private BookDTO book;
	private int quantity;
	private BigDecimal discount;
	private BigDecimal tax;
	
	
	public OrderBookDetailsDTO() {
	}

	public OrderBookDetailsDTO(OrderDetailsKey orderKey, OrderDTO order, BookDTO book, int quantity,
			BigDecimal discount, BigDecimal tax) {
		super();
		this.orderKey = orderKey;
		this.order = order;
		this.book = book;
		this.quantity = quantity;
		this.discount = discount;
		this.tax = tax;
	}

	public OrderDetailsKey getOrderKey() {
		return orderKey;
	}

	public void setOrderKey(OrderDetailsKey orderKey) {
		this.orderKey = orderKey;
	}

	public OrderDTO getOrder() {
		return order;
	}

	public void setOrder(OrderDTO order) {
		this.order = order;
	}

	public BookDTO getBook() {
		return book;
	}

	public void setBook(BookDTO book) {
		this.book = book;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public BigDecimal getTax() {
		return tax;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}
	
	
}
