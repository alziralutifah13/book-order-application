package com.example.BookOrderAplication.DTO;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.example.BookOrderAplication.model.OrderBookDetails;


public class OrderDTO {
	private Long orderID;
	private CustomerDTO customer;
	private Date orderDate;
	private BigDecimal totalOrder;
	private List<OrderBookDetailsDTO> orderBookDetails;
	
	public OrderDTO() {
	}

	public OrderDTO(Long orderID, CustomerDTO customer, Date orderDate, BigDecimal totalOrder,
			List<OrderBookDetailsDTO> orderBookDetails) {
		super();
		this.orderID = orderID;
		this.customer = customer;
		this.orderDate = orderDate;
		this.totalOrder = totalOrder;
		this.orderBookDetails = orderBookDetails;
	}

	public Long getOrderID() {
		return orderID;
	}


	public void setOrderID(Long orderID) {
		this.orderID = orderID;
	}


	public CustomerDTO getCustomer() {
		return customer;
	}


	public void setCustomer(CustomerDTO customer) {
		this.customer = customer;
	}


	public Date getOrderDate() {
		return orderDate;
	}


	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}


	public BigDecimal getTotalOrder() {
		return totalOrder;
	}


	public void setTotalOrder(BigDecimal totalOrder) {
		this.totalOrder = totalOrder;
	}

	public List<OrderBookDetailsDTO> getOrderBookDetails() {
		return orderBookDetails;
	}

	public void setOrderBookDetails(List<OrderBookDetailsDTO> orderBookDetails) {
		this.orderBookDetails = orderBookDetails;
	}
	
}
