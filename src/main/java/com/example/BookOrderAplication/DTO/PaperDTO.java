package com.example.BookOrderAplication.DTO;

import java.math.BigDecimal;
import java.util.List;

import com.example.BookOrderAplication.model.Publisher;

public class PaperDTO {
	private Long paperID;
	private String qualityName;
	private BigDecimal paperPrice;
	private List<Publisher> listPublisher;
	
	public PaperDTO() {
	}

	public PaperDTO(Long paperID, String qualityName, BigDecimal paperPrice, List<Publisher> listPublisher) {
		super();
		this.paperID = paperID;
		this.qualityName = qualityName;
		this.paperPrice = paperPrice;
		this.listPublisher = listPublisher;
	}

	public Long getPaperID() {
		return paperID;
	}

	public void setPaperID(Long paperID) {
		this.paperID = paperID;
	}

	public String getQualityName() {
		return qualityName;
	}

	public void setQualityName(String qualityName) {
		this.qualityName = qualityName;
	}

	public BigDecimal getPaperPrice() {
		return paperPrice;
	}

	public void setPaperPrice(BigDecimal paperPrice) {
		this.paperPrice = paperPrice;
	}

	public List<Publisher> getListPublisher() {
		return listPublisher;
	}

	public void setListPublisher(List<Publisher> listPublisher) {
		this.listPublisher = listPublisher;
	}
}
