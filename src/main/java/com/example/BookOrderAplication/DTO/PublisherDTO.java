package com.example.BookOrderAplication.DTO;

import java.util.List;

public class PublisherDTO {
	private Long publisherID;
	private String companyName;
	private String country;
	private PaperDTO paper;
	private List<BookDTO> listBooks;
	
	public PublisherDTO() {
		
	}

	public PublisherDTO(Long publisherID, String companyName, String country, PaperDTO paper, List<BookDTO> listBooks) {
		this.publisherID = publisherID;
		this.companyName = companyName;
		this.country = country;
		this.paper = paper;
		this.listBooks = listBooks;
	}

	public Long getPublisherID() {
		return publisherID;
	}

	public void setPublisherID(Long publisherID) {
		this.publisherID = publisherID;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public PaperDTO getPaper() {
		return paper;
	}

	public void setPaper(PaperDTO paper) {
		this.paper = paper;
	}

	public List<BookDTO> getListBooks() {
		return listBooks;
	}

	public void setListBooks(List<BookDTO> listBooks) {
		this.listBooks = listBooks;
	}
	
	
	
}
