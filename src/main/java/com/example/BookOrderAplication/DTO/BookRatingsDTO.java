package com.example.BookOrderAplication.DTO;


public class BookRatingsDTO {
	private Long bookRatingId;
	private BookDTO Book;
	private ReviewerDTO reviewer;
	private int ratingScore;
	
	
	public BookRatingsDTO() {
	}


	public BookRatingsDTO(Long bookRatingId, BookDTO book, ReviewerDTO reviewer, int ratingScore) {
		super();
		this.bookRatingId = bookRatingId;
		Book = book;
		this.reviewer = reviewer;
		this.ratingScore = ratingScore;
	}


	public Long getBookRatingId() {
		return bookRatingId;
	}


	public void setBookRatingId(Long bookRatingId) {
		this.bookRatingId = bookRatingId;
	}


	public BookDTO getBook() {
		return Book;
	}


	public void setBook(BookDTO book) {
		Book = book;
	}


	public ReviewerDTO getReviewer() {
		return reviewer;
	}


	public void setReviewer(ReviewerDTO reviewer) {
		this.reviewer = reviewer;
	}


	public int getRatingScore() {
		return ratingScore;
	}


	public void setRatingScore(int ratingScore) {
		this.ratingScore = ratingScore;
	}
	
	
}
